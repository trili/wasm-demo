<!--
SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>

SPDX-License-Identifier: MIT
-->

# Wasm Demo

Collection of scripts/materials needed for running the SCORU/Wasm demo.

# Sample debug log generation

It is possible to run sample debug log generation.

## Prerequisites

You should have `rust 1.60` installed, which can be done using the `rustup` tool.

## Running

You can then be able to run the following:
```shell
cd debug_log

cargo run
```

This will display two things:
1. the file path that is being written with the randomly generated debug logs.
2. the current TPS

To demonstrate this is working, you can run the following from another terminal:

```shell
# assuming log written to /run/user/1000/debug_log
tail -f -c 4 /run/user/1000/debug_log | xxd
```

This will then display the log being written, with output such as:
```
00016d60: f302 5240 3e00 425d 5100 476c e800 4229  ..R@>.B]Q.Gl..B)
00016d70: 4702 4784 e802 52ad 6c02 42de 0400 4277  G.G...R.l.B...Bw
00016d80: 1401 4740 a500 426d 7901 47ed a300 42e2  ..G@..Bmy.G...B.
00016d90: 8003 5293 9100 4217 d501 424c 5601 5280  ..R...B...BLV.R.
00016da0: 4103 4759 9803 4279 d001 52ce c602 42b7  A.GY..By..R...B.
00016db0: 8a01 47dd 6702 42ab 7a03 4269 d602 52d4  ..G.g.B.z.Bi..R.
00016dc0: 8802 4221 b803 5294 4a00 5295 1903 528a  ..B!..R.J.R...R.
00016dd0: 4301 4200 8001 5206 a400 4261 f101 4279  C.B...R...Ba..By
00016de0: ae00 4790 3f00 42c1 8503 42f4 8b00 4209  ..G.?.B...B...B.
```

