provider "google" {
  project     = var.project_id
  region      = "europe-west1"
  zone        = "europe-west1-b" # Saint-Ghislain, Belgique, Europe 	E2, N2, N2D, T2D, N1, M1, M2, C2
  credentials = var.gcp_creds
}