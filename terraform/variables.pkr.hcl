variable "project_id" {
  type        = string
  description = "The GCP project ID"
}

variable "zone" {
  type        = string
  description = "The GCP zone where the custom image resides."
}

variable "docker_registry_url" {
  type        = string
  description = "Google docker image registry (artifact registry) URL to authenticate with"
  default     = "europe-docker.pkg.dev"
}