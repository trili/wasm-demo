terraform {
  backend "gcs" {
    bucket = "blockchain-speedtest-368211-tfstate"
    prefix = "terraform/wasm-state"
  }
}
