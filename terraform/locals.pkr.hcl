locals {
  image_name          = "custom-cos-{{timestamp}}" # Add date
  image_family        = "custom-cos"
  machine_type        = "n1-standard-2"
  source_image_family = "cos-101-lts"
}