#!/bin/bash -xe

config_folder=/home/dockeruser/

# Create a docker user
useradd -m dockeruser -G docker

# Get files
curl -H 'Metadata-Flavor: Google' "http://metadata.google.internal/computeMetadata/v1/instance/attributes/docker-compose-yml" > $config_folder/docker-compose.yml
curl -H 'Metadata-Flavor: Google' "http://metadata.google.internal/computeMetadata/v1/instance/attributes/prometheus-yml" > $config_folder/prometheus.yml
curl -H 'Metadata-Flavor: Google' "http://metadata.google.internal/computeMetadata/v1/instance/attributes/web-yml" > $config_folder/web.yml
curl -H 'Metadata-Flavor: Google' "http://metadata.google.internal/computeMetadata/v1/instance/attributes/grafana-ini" > $config_folder/grafana.ini
mkdir $config_folder/grafana
curl -H 'Metadata-Flavor: Google' "http://metadata.google.internal/computeMetadata/v1/instance/attributes/datasources-yml" > $config_folder/grafana/datasources.yml
chown -R dockeruser:dockeruser $config_folder

# Run containers as dockeruser
sudo -u dockeruser bash << EOF

# Install docker-compose
curl -s -H 'Metadata-Flavor: Google' "http://metadata.google.internal/computeMetadata/v1/instance/attributes/install-docker-compose-sh" | bash

# Start Containers with docker-compose in container (not available on cos-101-lts)
docker run --rm -v /var/run/docker.sock:/var/run/docker.sock -v "$config_folder:$config_folder" -w="$config_folder" docker/compose:1.29.2 up -d

EOF