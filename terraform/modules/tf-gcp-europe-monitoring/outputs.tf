output "address" {
  value       = google_compute_address.this.address
  description = "IP address of the monitoring instance."
}

output "prometheus" {
  value       = local.prometheus_port
  description = "Port used by Prometheus."
}

output "grafana" {
  value       = local.grafana_port
  description = "Port used by Grafana."
}

output "user" {
  value       = local.user
  description = "User used in Prometheus and Grafana."
  sensitive   = true
}

output "password" {
  value       = local.password
  description = "Authentication Password"
  sensitive   = true
}

output "password_bcrypt" {
  value       = local.password_bcrypt
  description = "Password used in Prometheus and Grafana (bcrypt)."
  sensitive   = true
}