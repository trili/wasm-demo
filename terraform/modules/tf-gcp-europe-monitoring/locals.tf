locals {
  instance_name = "monitoring-${random_string.account-name.result}"
  machine_type  = "n2-standard-2"
  image         = "cos-cloud/cos-101-lts"
  labels        = {}
  tags = [
    "monitoring"
  ]
  metadata = {
    terraform   = "true"
    project_url = var.project_url
    # ssh-keys            = local.ssh-key
    startup-script = templatefile(
      "${path.module}/scripts/startup_script.sh", {

    })
    install-docker-compose-sh = file(
      "${path.module}/files/install-docker-compose.sh"
    )
    docker-compose-yml = templatefile(
      "${path.module}/templates/docker-compose.yml", {
        config_folder      = "/home/dockeruser"
        prometheus_port    = local.prometheus_port
        grafana_port       = local.grafana_port
        node_exporter_port = local.node_exporter_port
    })
    prometheus-yml = templatefile(
      "${path.module}/templates/prometheus.yml", {
        basic_auth_user     = local.user
        basic_auth_password = local.password
        self_instance_name  = local.instance_name
        prometheus_port     = local.prometheus_port
        grafana_port        = local.grafana_port
        node_exporter_port  = local.node_exporter_port
        project             = var.project_id
        zones               = var.monitored_zones
    })
    web-yml = templatefile(
      "${path.module}/templates/web.yml", {
        basic_auth_user            = local.user
        basic_auth_password_bcrypt = local.password_bcrypt
    })
    grafana-ini = templatefile(
      "${path.module}/templates/grafana.ini", {
        grafana_instance_name  = local.grafana_instance_name
        grafana_admin_user     = local.user
        grafana_admin_password = local.password
    })
    datasources-yml = templatefile(
      "${path.module}/templates/datasources.yml", {
        basic_auth_user     = local.user
        basic_auth_password = local.password
    })
  }

  prometheus_port    = 9090
  grafana_port       = 3000
  node_exporter_port = 9100

  network_tier = "PREMIUM"

  grafana_instance_name = "gcp-monitoring"
  user                  = var.prom_graf_user
  password              = var.prom_graf_password != "" ? var.prom_graf_password : random_password.basic_auth.result
  password_bcrypt       = var.prom_graf_password != "" ? bcrypt(var.prom_graf_password, 14) : bcrypt(random_password.basic_auth.result, 14)
}