resource "random_password" "basic_auth" {
  length      = 40
  min_lower   = 5
  min_numeric = 5
  min_upper   = 5
  special     = false
}

resource "random_string" "account-name" {
  length  = 5
  special = false
  upper   = false
}

resource "google_service_account" "this" {
  account_id   = "tf-generated-monitoring-${random_string.account-name.result}"
  display_name = "tf-generated-monitoring-${random_string.account-name.result}"
  description  = "Terraform generated service account for monitoring"
}

resource "google_project_iam_member" "this" {
  project = var.project_id
  role    = "roles/compute.viewer"
  member  = "serviceAccount:${google_service_account.this.email}"
}

resource "google_compute_address" "this" {
  name         = "monitoring-ip"
  address_type = "EXTERNAL"
  network_tier = local.network_tier
  region       = var.region
}

resource "google_compute_instance" "this" {
  name         = local.instance_name
  machine_type = local.machine_type
  labels       = local.labels
  tags         = local.tags
  metadata     = local.metadata
  zone         = var.instance_zone

  boot_disk {
    initialize_params {
      image = local.image
    }
  }

  network_interface {
    network            = var.network
    subnetwork         = var.subnetwork
    subnetwork_project = var.subnetwork_project
    access_config {
      nat_ip = google_compute_address.this.address
    }
  }

  service_account {
    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    # https://cloud.google.com/sdk/gcloud/reference/alpha/compute/instances/set-scopes#--scopes
    email  = google_service_account.this.email
    scopes = ["compute-ro"]
  }
}

resource "google_compute_firewall" "allow-http-monitoring" {
  name    = "default-allow-http-monitoring"
  network = var.network
  allow {
    protocol = "tcp"
    ports    = ["80"]
  }
  allow {
    protocol = "tcp"
    ports    = [local.prometheus_port]
  }
  allow {
    protocol = "tcp"
    ports    = [local.grafana_port]
  }
  allow {
    protocol = "tcp"
    ports    = [local.node_exporter_port]
  }
  source_ranges = ["0.0.0.0/0"]
  source_tags   = ["monitoring"]
}