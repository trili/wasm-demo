#!/bin/bash
VERSION=${1:-1.29.2}

echo "* Add an alias for docker-compose to the shell configuration file ..."
echo alias docker-compose="'"'docker run --rm \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v "$PWD:$PWD" \
    -w="$PWD" \
    docker/compose:'"${VERSION}"''"'" >> ~/.bashrc

echo "* Pull container image for docker-compose ..."
docker pull docker/compose:${VERSION}

echo "* Done"
echo "* To use docker-compose, run 'source ~/.bashrc' or simply re-login"