locals {
  /* instance */
  machine_type = "n1-standard-2"
  labels       = {}
  tags         = []
  metadata = {
    terraform           = "true"
    project_url         = var.project_url
    docker_registry_url = var.docker_registry_url
  }
  service_account = {
    email  = google_service_account.this.email
    scopes = ["cloud-platform"]
  }
  # Startup_script (max 256 kB)
  startup_script = templatefile(
    "./scripts/startup_script.sh", {
      docker_registry_url     = var.docker_registry_url
      nb_rollups_per_instance = var.nb_rollups_per_instance
      tps                     = var.tps
      port_base               = var.port_base
      nb_cols                 = var.nb_cols
    }
  )

  /* network */
  network            = ""
  subnetwork         = google_compute_subnetwork.this.self_link
  subnetwork_project = var.project_id
  subnet_cidr        = "10.10.0.0/16"
  network_tier       = "PREMIUM" # STANDARD or PREMIUM: https://cloud.google.com/network-tiers/docs/overview
  # For each address aka each instance, there is one of N access_config:
  access_config = [
    for addr in google_compute_address.this[*] : [{
      nat_ip       = addr.address
      network_tier = local.network_tier
    }]
  ]
  can_ip_forward     = "false"
  static_ips         = []
  service_port       = "80"
  rollups_port_range = "${var.port_base}-${var.port_base + var.nb_rollups_per_instance - 1}"
  named_ports = [
    {
      name = "service"
      port = local.service_port
    },
    {
      name = "ssh"
      port = "22"
    }
  ]

  /* image */
  source_image = ""
  # source_image_family  = "cos-101-lts" # Container-Optimized OS (COS): https://cloud.google.com/compute/docs/images/os-details#container-optimized_os_cos
  # source_image_project = "cos-cloud"
  source_image_family  = "custom-cos" # Build by Packer
  source_image_project = var.project_id

  # Filter to get the latest image built

  /* disks */
  disk_size_gb     = "100"
  disk_type        = "pd-standard"
  auto_delete      = "true"
  additional_disks = []
}