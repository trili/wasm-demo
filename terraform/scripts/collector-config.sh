#!/usr/bin/env bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# Parameters
############
NB_ROLLUPS_PER_INSTANCE=${nb_rollups_per_instance}
TPS=${tps}
PORT_BASE=${port_base}
NB_COLS=${nb_cols}

# Generate config file
######################

terraform output -json | jq '.instance_adresses.value' > output.json

ID=0
for ip in $(cat output.json | jq -r '.[]' | tr '\n' ' '); do
  for k in $(seq 1 "$NB_ROLLUPS_PER_INSTANCE"); do
      ROLLUP_ID=$(($ID * $NB_ROLLUPS_PER_INSTANCE + $k - 1))
      ROW=$(($ROLLUP_ID / $NB_COLS))
      COL=$(($ROLLUP_ID % $NB_COLS))
      PORT=$(($PORT_BASE + $k - 1))
      cat <<EOF
     { "host" : "$ip", "port" : $PORT, "row" : $ROW, "column" : $COL }
EOF
  done
  ID=$(($ID + 1))
done | jq -s '[.[]]' > config.json
