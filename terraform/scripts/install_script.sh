#!/bin/bash -xe

# For monitoring purpose
docker pull prom/node-exporter:latest
sudo mv /tmp/node-exporter.service /var/

ROLLUP_DATA_DIR=/home/data
sudo mkdir $ROLLUP_DATA_DIR
cd $ROLLUP_DATA_DIR
sudo wget -c https://gitlab.com/trili/wasm-demo/-/raw/main/preimages/wasm_2_0_0.tar.gz -O - | sudo tar -xz

docker-credential-gcr configure-docker --registries=${docker_registry_url}
docker pull ${docker_registry_url}/blockchain-speedtest-368211/default-repo/pewulfman/tezos-rollup-celebration:collector
