output "umig_self_links" {
  description = "List of self-links for unmanaged instance groups"
  value       = module.vm_umig.self_links
}

output "available_zones" {
  description = "List of available zones in region"
  value       = module.vm_umig.available_zones
}

output "instance_template_self_link" {
  description = "Self-link of instance template"
  value       = module.vm_instance_template.self_link
}

output "tags" {
  description = "Tags that will be associated with instance(s)"
  value       = module.vm_instance_template.tags
}

output "instance_adresses" {
  description = "Instances addresses"
  value       = google_compute_address.this.*.address
}

# Monitoring

output "address" {
  value       = module.monitoring.address
  description = "IP address of the monitoring instance."
}

output "prometheus" {
  value       = module.monitoring.prometheus
  description = "Port used by Prometheus."
}

output "grafana" {
  value       = module.monitoring.grafana
  description = "Port used by Grafana."
}

output "user" {
  value       = module.monitoring.user
  description = "User used in Prometheus and Grafana."
  sensitive   = true
}

output "password" {
  value       = module.monitoring.password
  description = "Authentication Password"
  sensitive   = true
}

output "password_bcrypt" {
  value       = module.monitoring.password_bcrypt
  description = "Password used in Prometheus and Grafana (bcrypt)."
  sensitive   = true
}