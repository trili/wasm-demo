# Preimages

DAC preimages required for the demo. These should be unzipped and placed in: `$ROLLUP_DATA_DIR/wasm_2_0_0`. The preimages are a mixture of 'message preimages', and 'tx kernel' preimages (required for the installer).

Currently only the message-preimages for a single rollup are included.
