## Requirements

```
opam install graphics
```

## Build

```
make
```

## Usage

```
./img2inputs 7 3 img/logo.ppm img/ocaml.ppm
```

will create CSV files corresponding to transactions to move from the first image to the second.

```
./inputs2img 7 3 img/logo.ppm img/ocaml-2.ppm
```

reconstruct `ocaml.ppm` from `logo.ppm` by applying the differences stored in the CSV files.
