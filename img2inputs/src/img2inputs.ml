open Libimage

let args = Array.to_list Sys.argv |> List.tl

let () =
  if List.length args < 4 then
    failwith "img2inputs [nb_rows] [nb_cols] [from.ppm] [to.ppm]"

let nb_rows, nb_cols, from_image, to_image =
  match args with
  | [nb_row; nb_col; from_image; to_image] ->
    (int_of_string nb_row, int_of_string nb_col, from_image, to_image)
  | _ ->
    failwith "Invalid number of arguments"

let rec ( -- ) start stop = if start = stop then [start] else start :: (start + 1 -- stop)

let couts =
  List.map (fun row ->
      List.map (fun col ->
          open_out (Format.asprintf "input-%02d-%02d.csv" row col)
        ) (0 -- (nb_cols - 1))
      |> Array.of_list)
    (0 -- (nb_rows - 1)) |> Array.of_list

let from_image = Image.load from_image

let to_image = Image.load to_image

let () =
  assert (from_image.Image.w = to_image.Image.w);
  assert (from_image.Image.h = to_image.Image.h)

let height_cell = from_image.Image.h / nb_rows

let () = if from_image.Image.h mod nb_rows <> 0 then failwith "The height must be a multiple of nb_rows"

let () = if from_image.Image.w mod nb_cols <> 0 then failwith "The width must be a multiple of nb_cols"

let width_cell = from_image.Image.w / nb_cols


let account_of x y =
   let x0 = x mod width_cell
   and y0 = y mod height_cell in
   y0 * width_cell + x0

let () =
  Image.iter from_image @@ fun x y from_color ->
      let to_color = Image.get to_image x y in
      let row = y / height_cell
      and col = x / width_cell in
      let cout = couts.(row).(col) in
      let account = account_of x y in
      let red_diff = Image.red to_color - Image.red from_color
      and green_diff = Image.green to_color - Image.green from_color
      and blue_diff = Image.blue to_color - Image.blue from_color
      in
        Printf.fprintf cout "%d,%d,%d,%d\n"
          account red_diff green_diff blue_diff

let () =
  Array.(iter (iter close_out) couts)
