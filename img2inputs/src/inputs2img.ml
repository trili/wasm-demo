open Libimage

let args = Array.to_list Sys.argv |> List.tl

let () =
  if List.length args < 4 then
    failwith "inputs2img [nb_row] [nb_col] [from.ppm] [to.ppm]"

let nb_rows, nb_cols, from_image, to_image_filename =
  match args with
  | [nb_row; nb_col; from_image; to_image] ->
    (int_of_string nb_row, int_of_string nb_col, from_image, to_image)
  | _ ->
    failwith "Invalid number of arguments"

let rec ( -- ) start stop = if start = stop then [start] else start :: (start + 1 -- stop)

let from_image = Image.load from_image

let to_image = Image.(make from_image.w from_image.h (fun _ _ -> black))

let height_cell = from_image.Image.h / nb_rows

let width_cell = from_image.Image.w / nb_cols

let coord_of account =
  account mod width_cell, account / width_cell

let () =
  List.iter (fun row ->
      List.iter (fun col ->
          let cin = open_in (Format.asprintf "input-%02d-%02d.csv" row col) in
          let rec process () =
            let line =
              try
                Some (input_line cin)
              with End_of_file -> None
            in
            match line with
            | None -> ()
            | Some line ->
              match String.split_on_char ',' line with
              | [account; red_diff; green_diff; blue_diff] ->
                let rd = int_of_string red_diff
                and gd = int_of_string green_diff
                and bd = int_of_string blue_diff in
                let (xc, yc) = coord_of (int_of_string account) in
                let (x, y) = (width_cell * col + xc, height_cell * row + yc)  in
                let c0 = Image.get from_image x y in
                let (r0, g0, b0) = Image.(red c0, green c0, blue c0) in
                let c = Image.rgb (r0 + rd) (g0 + gd) (b0 + bd) in
                Image.put to_image x y c;
                process ()
              | _ ->
                failwith "Invalid CSV file"
          in
          process ()
        ) (0 -- (nb_cols - 1)))
    (0 -- (nb_rows - 1));
  Image.write to_image_filename to_image
